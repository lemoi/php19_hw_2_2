<?php
    $path = implode(DIRECTORY_SEPARATOR, [__DIR__, "tests", ""]);
    $files = glob($path . "*.json");
    $template = '<li><a href="test.php?name=%1">Тест %1</a></li>';
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>PHP-19. Task 2.2</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
    <div class="nav">
        <a href="admin.php">Добавить тест</a>
        <a href="list.php">Выбрать тест</a>
        <a href="test.php">Пройти тест</a>
        <hr>
    </div>
    <h2>Список тестов</h2>
    <ul>
        <?php foreach($files as $fname) {
            echo str_replace("%1", basename($fname, ".json"), $template);
        } ?>
    </ul>
</body>
</html>
