<?php
    $path = implode(DIRECTORY_SEPARATOR, [__DIR__, "tests", ""]);
    if(!array_key_exists("name", $_GET)) {
        $file = glob($path . "*.json")[0];
    } else {
        $file = $path . $_GET["name"] . ".json";
    }

    if(!(file_exists($file)&&is_file($file))) {
        echo "<h1>Ошибка 404</h1>Нет такой страницы";
        return;
    }

    $testName = basename($file, ".json");
    $json = file_get_contents($file);
    $test = json_decode($json, true);
    $tmpQuestion = '<h2>%3 Вопрос %1. %2</h2>';
    $tmpAnswer = '<input type="radio" name="%1" value="%2"><span style="%4">%3</span><br>';
    $tmpResult = '<h2>Верных: %1 (из %2 вопросов)</h2>';
    $sent = isset($_POST)&&count($_POST);
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>PHP-19. Task 2.2</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
    <div class="nav">
        <a href="admin.php">Добавить тест</a>
        <a href="list.php">Выбрать тест</a>
        <a href="test.php">Пройти тест</a>
        <hr>
    </div>
    <form method="post">
        <h1>Тест <?php echo $testName; ?></h1>
        <?php
            $i = 1;
            $correctAnswer = 0;
            foreach ($test as $question) {
                $answerName = "answer" . $i;
                if(!$sent) {
                    $questionResult = "";
                } elseif (array_key_exists($answerName, $_POST)&&($_POST[$answerName] === $question["correct"])) {
                    $questionResult = "&#10004;";
                    $correctAnswer++;
                } else {
                    $questionResult = "&#10008;";
                }
                $tag = str_replace(["%1", "%2", "%3"], [$i, htmlspecialchars($question["question"]), $questionResult], $tmpQuestion);
                echo $tag;
                foreach ($question["answer"] as $key => $answer) {
                    if(!$sent) {
                        $styleAnswer = "";
                    } else {
                        $styleAnswer = $key === $question["correct"] ? "color: green;": "color: black;";
                        $styleAnswer .= array_key_exists($answerName, $_POST)&&($_POST[$answerName] === $key) ? "background-color: yellow;": "";
                    }
                    echo str_replace(["%1", "%2", "%3", "%4"], [$answerName, htmlspecialchars($key), htmlspecialchars($answer), $styleAnswer], $tmpAnswer);
                }
                $i++;
            }
        ?>
        <hr>
        <input type="submit" value="Отправить">
    </form>
    <?php
        if($sent) {
            echo str_replace(["%1", "%2"], [$correctAnswer, count($test)], $tmpResult);
        }
    ?>
</body>
</html>

