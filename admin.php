<?php
    /* Домашнее задание к лекции 2.2 «Обработка форм»

    Генератор тестов на PHP и JSON:
        1. Создать файл admin.php с формой, через которую на сервер можно загрузить JSON-файл c тестом.
        2. Создать файл list.php со списком загруженных тестов.
        3. Создать файл test.php, который:
            - Принимает в качестве GET-параметра номер теста и отображает форму теста.
            - Если форма отправлена, проверяет и показывает результат.

    Структура теста
        - Структура JSON полностью на ваше усмотрение.
        - Тест — это несколько вопросов.
        - Вопрос — это текст вопроса, плюс несколько вариантов ответа. Один или несколько вариантов помечены как правильные.

    Пример теста:

    1. Сколько граммов в одном килограмме?
        - 10
        - 100
        - 1000
        - 10000

    2. Сколько метров в одном дециметре?
        - 100
        - 10
        - 0.1
        - 0.01

    */
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>PHP-19. Task 2.2</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
    <div class="nav">
        <a href="admin.php">Добавить тест</a>
        <a href="list.php">Выбрать тест</a>
        <a href="test.php">Пройти тест</a>
        <hr>
    </div>
    <form method="post" enctype="multipart/form-data">
        <input type="file" name="userfile">
        <input type="submit" name="" value="Отправить">
    </form>
</body>
</html>

<?php
    function fileIsValid($path) {
        // Возвращаем true в случае корректного теста, в противном случае описание ошибки
        $content = file_get_contents($path);
        if(!$content) {
            return "Не удалось получить содержимое файла";
        }
        $tests = json_decode($content, true);
        if(!is_array($tests)) {
            return "Файл с тестом должнен быть json содержащим массив вопросов";
        }
        $importantFields = ["question", "answer", "correct"];
        $i = 1;
        $prefix = "Вопрос {$i}. ";
        foreach ($tests as $test) {
            foreach ($importantFields as $field) {
                if(!array_key_exists($field, $test)) {
                    return $prefix . "Отсутствует обязательное поле {$field}";
                }
            }
            if(!(is_string($test["question"])&&(is_string($test["correct"])||is_int($test["correct"])))) {
                return $prefix . "Поле question - должно содержать описание вопроса, а correct - ключ корректного ответа в массиве answer";
            }
            if(!(is_array($test["answer"])&&(array_key_exists($test["correct"], $test["answer"])))) {
                return $prefix . "В списке ответов на вопрос отсутствует ключ верного ответа указанный в поле correct";
            }
            $i++;
        }
        return true;
    }

    if(isset($_FILES) and array_key_exists("userfile", $_FILES)) {
        $file = $_FILES["userfile"];
        $result = fileIsValid($file["tmp_name"]);
        if($result !== true) {
            echo $result;
            return;
        }

        $path = implode(DIRECTORY_SEPARATOR, [__DIR__, "tests", $file["name"]]);
        if(move_uploaded_file($file["tmp_name"], $path)) {
            echo "Файл " . $file["name"] . " передан <br>";
        } else {
            echo "Файл не передан";
        }
    }
?>
